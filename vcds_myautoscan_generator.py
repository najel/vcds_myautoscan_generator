#-------------------------------------------------------------------------------
# Name:        VCDS_MyAutoScan_Generator
# Purpose:     Read VCDS autoscan log or control module finder file
#              and write control modules to myautoscan.txt
# Author:      Philipp Nagel
#
# Created:     21/11/2016
# Copyright:   (c) Philipp Nagel 2016
# "VCDS" is a Registered Trademark of Ross-Tech, LLC.
# I am not affiliated with Ross-Tech, LLC. This tool is hobby project I wrote
# to make my life easier.
#
# Licence:     GNU General Public License v3.0
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

#import re
#re.split(r':+|        ',st)

def main():
    inFile = r'C:\Users\PhilippN\Downloads\Log-3VWRK69M43M059447-243470km-151285mi.txt'
    myAutoScan = r'C:\Ross-Tech\VCDS\MyAutoScan.txt'


    addresses = []

    with open(inFile) as f:
        for l in f.readlines():
            if l.startswith("Address"):
                address = l.split()[1].rstrip(':')
                addresses.append(address)

    code = raw_input('Enter Car Code:')
    description = raw_input('Enter Car Description')

    addresses_string = ','.join(addresses)

    ret = ','.join([str(code), str(description), addresses_string])

    with open(myAutoScan, 'a') as autoscanfile:
        autoscanfile.write(ret)

if __name__ == '__main__':
    main()
