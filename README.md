Read VCDS autoscan log or control module finder file nd write control modules to myautoscan.txt
This is useful for cars that do not have CAN controller that returns a list of installed modules.
Adding the installed modules to the myautoscan.txt speeds up autoscans since typically, there are many modules that aren't installed, but VCDS still tried their address.

When adding a vehicle to myautoscan.txt, only the addresses listed there are tried.

To use this, you first have to run an autoscan for the car, or run the Find Control Modules tool. Use this file to generate the entry in myautoscan.txt, and the next autoscan will be much faster.

Inputs:

inFile: Autoscan log file or control module finder file of the car in question
myAutoScan: path of the myautoscan.txt file

code: "Chasses Type" for the vehicle you are adding.
description: Long form descriptor of the vehicle you are adding

ToDo:
- Tkinter GUI
- File Selector
- Automatically locate myautoscan.txt file